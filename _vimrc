 " ---------------------------------------------------------------------------
" VIM SETTINGS-2012.07.17
" ---------------------------------------------------------------------------
" dein -----------------------------------------------------------------
filetype off

if &compatible
  set nocompatible
endif
set runtimepath+=~/.vim/dein/repos/github.com/Shougo/dein.vim

call dein#begin(expand('~/.vim/dein'))

call dein#add('joker1007/vim-markdown-quote-syntax')
call dein#add('tyru/open-browser.vim')
call dein#add('kannokanno/previm')
call dein#add('itchyny/lightline.vim')
call dein#add('Shougo/neomru.vim')
call dein#add('Shougo/vimshell')
call dein#add('h1mesuke/unite-outline')
call dein#add('Shougo/unite.vim')
call dein#add('vim-scripts/JavaScript-syntax')
call dein#add('kchmck/vim-coffee-script')
call dein#add('pangloss/vim-javascript')
call dein#add('Shougo/neocomplcache')
call dein#add('vim-scripts/sudo.vim')
call dein#add('vim-scripts/surround.vim')
call dein#add('vim-scripts/The-NERD-Commenter')
call dein#add('thinca/vim-quickrun')
call dein#add('thinca/vim-ref')
call dein#add('wavded/vim-stylus')
call dein#add('digitaltoad/vim-jade')
call dein#add('briancollins/vim-jst')
call dein#add('tpope/vim-rails')
call dein#add('vim-scripts/JSON.vim')
call dein#add('vim-scripts/wombat256.vim')
call dein#add('tsukkee/unite-help')
call dein#add('lambdalisue/vim-gista')

call dein#end()

filetype plugin indent on

" Plugin --------------------------------------------------------------------

" vim-gista
let g:gista#github_user = 'hilott'
let g:gista#update_on_write = 1

" NERD_Commenter
let g:NERDCreateDefaultMappings = 0
let g:NERDSpaceDelims = 1
let g:NERDShutUp=1
nmap <silent> ,/ <Plug>NERDCommenterToggle

" neocomplcache
let g:neocomplcache_enable_at_startup = 1
let g:neocomplcache_auto_completion_start_length = 3
let g:neocomplcache_enable_smart_case = 1
let g:neocomplcache_skip_auto_completion_time = '0.5'

" Unite
let g:unite_enable_split_vertically = 0
let g:unite_winwidth = 48
let g:unite_split_rule = "rightbelow"
let g:unite_source_history_yank_enable = 1
nmap <C-o> :Unite file<CR>
nmap <C-l> :Unite buffer<CR>
nmap <C-f> :Unite file_mru<CR>
nmap <C-b> :Unite bookmark<CR>
nmap <C-y> :Unite history/yank<CR>

" vimshell
nnoremap <silent> ,s :VimShellPop<CR>

" Keymap --------------------------------------------------------------------

" 移動3倍
noremap <C-j> 3+zz
noremap <C-k> 3-zz
" 表示行で移動
nnoremap j gj
nnoremap k gk
nnoremap Y y$
nnoremap R gR
" バッファ移動
nnoremap <silent> ,bb :b#<CR>
nnoremap <silent> ,bp :bp<CR>
nnoremap <silent> ,bn :bn<CR>
nnoremap <silent> ,bd :bd<CR>
" 選択文字列置換
vnoremap s y:%s/<C-R>"//g<Left><Left>
" C-bで改行ミス取り消し
inoremap <C-b> <Esc>k$Ji 
nmap ,w :x<CR>
nmap ,q :q!<CR>
" 検索でカーソル位置をセンターに
nmap n nzz 
nmap N Nzz 
nmap * *zz 
nmap # #zz 
nmap g* g*zz 
nmap g# g#zz
" 貼り付け時のインデントあわせ
nnoremap <Esc>P P'[v']=
nnoremap <Esc>p p'[v']=
" ペーストモードトグル
set pastetoggle=<F9>
" 検索ハイライト解除
nmap <ESC><ESC> :noh<CR>
" utf-8で開き直す
nmap <C-@> :e ++enc=utf-8<CR>
" vimrc 読み直し
nnoremap <Silent> <Leader>r :source ~/.vimrc<CR>
" c-hでesc
imap <C-[> <ESC>


" File --------------------------------------------------------------------

set hidden
set autoread
set isfname-==
" 2個上のディレクトリ以下から再帰的に探す
set tags=+../../**/tags
" 補完設定
set wildmenu
set wildmode=list:longest,full
" Backup, Swap
set directory=/tmp
set nobackup


" Edit ------------------------------------------------------------------- 

set autoindent smartindent
"set smarttab
set ignorecase
set smartcase
"set tabstop=4
"set softtabstop=4
"set shiftwidth=4
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set cinoptions=t0,:0,g0,(0
set backspace=indent,eol,start
set formatoptions=tcqnmM
set iskeyword+=-,!,?
"set indentkeys-=0#
set virtualedit=block


" Move ----------------------------------------------------------------

set showmatch matchtime=1
set matchpairs+=<:>
"set whichwrap+=h,l,<,>,[,],b,s
set mouse=a
"if has('mouse')
"    set mouse=h
"endif

" Display -------------------------------------------------------------

set ruler
set ruf=%45(%12f%=\ %m%{'['.(&fenc!=''?&fenc:&enc).']'}\ %l-%v\ %p%%\ [%02B]%)
set statusline=%f\ %m%=%{(&fenc!=''?&fenc:&enc).':'.&ff}%{(&list?'>':'')}\ %l/%L-%v\ %p%%\ [%02B]
set showcmd
set cmdheight=1
set laststatus=2
set number
set shortmess+=I
set vb t_vb=
set wrap
set t_Co=256
set list
set listchars=tab:>-
set hlsearch
set cursorline
syntax on
" Not sudo user
if system('id -u') != 0
  colorscheme wombat256mod
  " hi SpecialKey ctermfg=237 ctermbg=234 cterm=none
endif


" History, Compement ----------------------------------------------------

set history=50
set wildmenu


" Window ---------------------------------------------------------------

set splitbelow
set splitright
set sessionoptions+=resize
set previewheight=5

" Filetype ------------------------------------------------------------------

au BufRead,BufNewFile *.coffee set filetype=coffee
au BufRead,BufNewFile *.jade set filetype=jade
au BufRead,BufNewFile *.json set filetype=json
"au BufRead,BufNewFile *.scss set filetype=scss
"autocmd FileType javascript :compiler gjslint
autocmd QuickfixCmdPost make copen

" markdown
au BufRead,BufNewFile *.md set filetype=markdown
au BufRead,BufNewFile *.md set tabstop=4
au BufRead,BufNewFile *.md set softtabstop=4
au BufRead,BufNewFile *.md set shiftwidth=4
au BufRead,BufNewFile *.md set nonumber
au BufRead,BufNewFile *.md nmap ,p :PrevimOpen<CR>

" gradle
au BufRead,BufNewFile *.gradle set filetype=groovy

" Character code ------------------------------------------------------------

if &encoding !=# 'utf-8'
  set encoding=japan
  set fileencoding=japan
endif
if has('iconv')
  let s:enc_euc = 'euc-jp'
  let s:enc_jis = 'iso-2022-jp'
  " iconvがeucJP-msに対応しているかをチェック
  if iconv("\x87\x64\x87\x6a", 'cp932', 'eucjp-ms') ==# "\xad\xc5\xad\xcb"
    let s:enc_euc = 'eucjp-ms'
    let s:enc_jis = 'iso-2022-jp-3'
  " iconvがJISX0213に対応しているかをチェック
  elseif iconv("\x87\x64\x87\x6a", 'cp932', 'euc-jisx0213') ==# "\xad\xc5\xad\xcb"
    let s:enc_euc = 'euc-jisx0213'
    let s:enc_jis = 'iso-2022-jp-3'
  endif
  " fileencodingsを構築
  if &encoding ==# 'utf-8'
    let s:fileencodings_default = &fileencodings
    let &fileencodings = s:enc_jis .','. s:enc_euc .',cp932'
    let &fileencodings = &fileencodings .','. s:fileencodings_default
    unlet s:fileencodings_default
  else
    let &fileencodings = &fileencodings .','. s:enc_jis
    set fileencodings+=utf-8,ucs-2le,ucs-2
    if &encoding =~# '^\(euc-jp\|euc-jisx0213\|eucjp-ms\)$'
      set fileencodings+=cp932
      set fileencodings-=euc-jp
      set fileencodings-=euc-jisx0213
      set fileencodings-=eucjp-ms
      let &encoding = s:enc_euc
      let &fileencoding = s:enc_euc
    else
      let &fileencodings = &fileencodings .','. s:enc_euc
    endif
  endif
  " 定数を処分
  unlet s:enc_euc
  unlet s:enc_jis
endif
" 日本語を含まない場合は fileencoding に encoding を使うようにする
if has('autocmd')
  function! AU_ReCheck_FENC()
    if &fileencoding =~# 'iso-2022-jp' && search("[^\x01-\x7e]", 'n') == 0
      let &fileencoding=&encoding
    endif
  endfunction
  autocmd BufReadPost * call AU_ReCheck_FENC()
endif
" 改行コードの自動認識
set fileformats=unix,dos,mac
" □とか○の文字があってもカーソル位置がずれないようにする
if exists('&ambiwidth')
  set ambiwidth=double
endif

" for MacVim
if has('gui_macvim')
  set transparency=3
  set guifont=Menlo:h12
  set lines=90 columns=160
  set guioptions-=T
endif

" for memo
let s:is_mac = (has('mac') || has('macunix') || has('gui_macvim') || system('uname') =~? '^darwin')
let mapleader = ","

function! s:open_memo_file()"
    let l:memo_dir = $HOME.'/Dropbox/memo/'
    if !isdirectory(l:memo_dir)
        call mkdir(l:memo_dir, 'p')
    endif

    let l:filename = l:memo_dir . strftime('%Y-%m-%d') . '.md'

    execute 'edit ' . l:filename
    execute 'set fenc=utf-8'
    execute '999'
    execute 'write'
endfunction augroup END"

command! -nargs=0 MemoNow call s:open_memo_file()
command! -nargs=0 MemoList :Unite file:/Users/hal/Dropbox/memo/ -buffer-name=memo_list
command! -nargs=0 MemoGrep :Unite grep:/Users/hal/Dropbox/memo/ -no-quit
command! -nargs=0 MemoFiler :VimFiler MEMO_DIR

nnoremap [memo] <Nop>
nmap <Leader>m [memo]
nnoremap <silent> [memo]n :MemoNow <CR>
nnoremap <silent> [memo]l :MemoList <CR>
nnoremap <silent> [memo]f :MemoFiler <CR>
nnoremap <silent> [memo]g :MemoGrep <CR>

" for todo
let s:is_mac = (has('mac') || has('macunix') || has('gui_macvim') || system('uname') =~? '^darwin')
function! s:open_todo_file()"
    let l:todo_dir = $HOME.'/Dropbox/todo/'
    if !isdirectory(l:todo_dir)
        call mkdir(l:todo_dir, 'p')
    endif

    let l:filename = l:todo_dir . strftime('todo') . '.md'

    execute 'edit ' . l:filename
    execute 'set fenc=utf-8'
    execute '999'
    execute 'write'
endfunction augroup END"

command! -nargs=0 TodoOpen call s:open_todo_file()
nmap <Leader>t :TodoOpen<CR>

" ----- CLIPBOARD
if s:is_mac
    vmap <C-c> y:call system("pbcopy", getreg("\""))<CR>
    vmap <D-c> y:call system("pbcopy", getreg("\""))<CR>
    nmap <Space><C-v> :call setreg("\"",system("pbpaste"))<CR>p
    nmap <Space><D-v> :call setreg("\"",system("pbpaste"))<CR>p
endif
set clipboard+=unnamed

" --- test
function! s:gista_new_file()"
  let l:filename = l:memo_dir . strftime('%Y-%m-%d') . '.md'
  :Gista -p -d l:filename
endfunction augroup END"
command! -nargs=0 GistaNow call s:gista_new_file()
nmap <Leader>gn :GistaNow<CR>
