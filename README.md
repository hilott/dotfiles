dotfiles
===
dotfiles...

Setup Vim
---

1.Setup NeoBundle

    $ mkdir -p ~/.vim/bundle
    $ git clone git://github.com/Shougo/neobundle.vim ~/.vim/bundle/neobundle.vim

2.Clone dotfiles

    $ mkdir ~/Git
    $ cd ~/Git
    $ git clone git@bitbucket.org:hilott/dotfiles.git

3.Create symbolic link

    $ ln -s ~/Git/dotfiles/_vimrc ~/.vimrc

4.Install vim plugins

Launch vim, run :NeoBundleInstall

5.Compile vimproc

    $ cd ~/.vim/bundle/vimproc
    $ make -f make_mac.mak
